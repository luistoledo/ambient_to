class Button
{

  int x, y, w, h, r;
  int state;
  color fillColor;
  color strokeColor;
  ArrayList<Sound> sounds;
  int currentSound = 0;
  String name;
  color col;
  color normalBorderColor;
  String soundFile;
  var c = 0; // counter for the display function

  Button (int ix, int iy, int iw, int ih)
  {
    r = 40;
    x = ix;
    y = iy;
    w = iw;
    h = ih;
    fillColor = color(204, 153, 0);
    strokeColor = color(240, 240, 240);
    normalBorderColor = color(255, 240); 
  }

  void loadDictionary(ArrayList<Sound> dic)
  {
    sounds = new ArrayList<Sound>();
    for (int i=0; i<=dic.size()-1; i++)
    {
      sounds.add(dic.get(i));
    }
    fillColor = color(<Sound>sounds.get(currentSound).col);
  }
  
  void clearDictionary()
  {
    sounds = new ArrayList<Sound>();
    name = ""; 
    soundFile = ""; 
    col = color(100, 100, 100);
  }

  void draw()
  {
    pushStyle();
    fill (fillColor, 80);
    stroke(strokeColor, 160); //stroke(220);
    rect(x, y, w, h, r);
    fill(250);   
    textSize(12); 
    textAlign(CENTER, CENTER);
    text(name, x, y, w, h-4);
    popStyle();
  }
  
  void drawVisuals()
  {
    if (sounds.get(currentSound).display)
    {
      c = sounds.get(currentSound).display(x,y,c) || c;
    }
  }

  Boolean isIn(int px, int py)
  {
    if (px > x && px < x+w) {
      if (py > y && py < y+h) {
        return true;
      }
    }
    return false;
  }

  Boolean wasPressed = false;
  int px, py;
  void pressed(int ix, int iy)
  {
    px = x-ix; 
    py = y-iy;
    wasPressed = true;
    wasDragged = false;

//    strokeColor = normalBorderColor;
    strokeColor = col;
  }

  void released()
  {
    wasPressed = false;

    if (!wasDragged)
    {
      stopSound();
      if (currentSound >= sounds.size()-1) {
        currentSound=0;
      } 
      else {
        currentSound++;
      }
      playSound();
    }

    setPublicSoundData();
    fillColor = col;
    strokeColor = col;
  }

  Boolean wasDragged = false;
  void dragged(int ix, int iy)
  {
    wasDragged = true;
    x = ix + px;
    y = iy + py;
    updateSoundSpeed();
    updateSoundVolume();
  }

  void setPublicSoundData()
  {
    name = <Sound>sounds.get(currentSound).name;
    col = <Sound>sounds.get(currentSound).col;
    soundFile = <Sound>sounds.get(currentSound).file;
  }
    
  void stopSound()
  {
    //sounds.get(currentSound).sound.stop();
    sounds.get(currentSound).sound.cue(0);
    console.log("recue: " + sounds.get(currentSound).name);
  }

  void updateSoundSpeed()
  {
    float speed = map(x+w/2, 0, width, 0.2, 1.5); 
    sounds.get(currentSound).sound.speed(speed);
  }
  
  void updateSoundVolume()
  {
    float volume = map(y+h/2, 0, height, 1, -0.1); 
    sounds.get(currentSound).sound.volume(volume);    
  }
  
  void playSound()
  {
    updateSoundVolume();
    updateSoundSpeed();
    sounds.get(currentSound).sound.play();
    console.log("play: " + sounds.get(currentSound).name);
  }
}

