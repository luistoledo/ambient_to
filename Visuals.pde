class Visuals
{

  Visuals()
  {
    setupChat();
    setupBird();
    setupOm();
  }

  static void drawRain(int x, int y, var c)
  {
    int q = map(y, 0, height, 4, 0.5) * map(x, 0, width, 0.5, 4);
    pushStyle();
    fill(250);
    for (int i=0; i<=q; i++)
    {
      int dx = int(random(width));
      int dy = height - int(random(0, 20));

      stroke(color(int(random(50, 250))));
      strokeWeight(1);

      line(dx, 0, dx, dy);

      noStroke();
      ellipse(dx+1, dy+1, 2, 2);
    }
    popStyle();
  }

  static void drawNoise(int x, int y, var c)
  {
    float noiseSclae = 0.02;
    int q = map(y, 0, height, 40, 0);
    if (q > f_f) { //not so sure..but..
      pushStyle();
      for (int i=0; i<=q; i++)
      {
        int c = int(random(0, 255));
        int x = int(random(0, width));
        int y = int(random(0, height));
        fill(c, 230);
        noStroke();
        rect(x, y, 3, 3);
        //      point(x, y);
      }
      popStyle();
    }
  }

  PImage[] chatImages = new PImage[20];
  void setupChat()
  {
    for (int i=0; i<=20; i++)
    {
      chatImages[i] = loadImage("chat/movie"+i+".jpg");
    }
  }
  void drawChat(int x, int y, var o)
  {
    int t = map(y, 0, height, 60, 0);
    float q = map(x, 0, width, 1.2, .034);
    if (ceil(frameCount % (frameRate*q)) == 1.0)
    {
      o++;
      if (o > 18) { 
        o = 1;
      }
    }
    pushStyle();
    tint(255, t);
    image(chatImages[o], 0, 0, width, height);
    popStyle();

    return o;
  }


  PImage[] birdImages = new PImage[12];
  int currentBird = 0;
  int birdCounter = 0;
  void setupBird()
  {
    for (int i=0; i<=11; i++)
    {
      birdImages[i] = loadImage("bird/bird_"+i+".png");
    }
  }
  void drawBird(int x, int y, var o)
  {
    int t = map(y, 0, height, 100, 0);
    var q = map (x, 0, width, 1, 0.01) * frameRate;

    birdCounter++;
    if (birdCounter >= q)
    {
      birdCounter = 0;
      currentBird = (currentBird >= birdImages.length()-1) ? 1 : (currentBird+1);
    }

    pushStyle();
    tint(255, t);
    scale(4.0);
    image(birdImages[currentBird], 0, 0);
    popStyle();

    return o;
  }


  PImage omImage;
  void setupOm()
  {
    omImage = loadImage("Amitayus_Mandala.jpeg");
  }
  void drawOm(int x, int y, var c)
  {
    int t = map(y, 0, height, 60, 0);
    float r = map(x, 0, width, 0.05, 1) * v0;
    float s = (map(x, 0, width, 1, 2) * v00) + 1.5;

    pushMatrix();
    imageMode(CENTER);
    translate(width_2, height_2);
    rotate(r);
    scale(s);
    tint(255, t);
    image(omImage, 0, 0);
    popMatrix();
    imageMode(CORNER);
  }


  int bellCounter = 0;
  void drawBell(int x, int y)
  {
    float t = map(y, 0, height, 40, 5);
    //    float q = map(x, 0, width, 0.2, 1.5) * frameRate / 4 * t5;
    float s = map(x, 0, width, 1.5, 0.2) * frameRate * 8.05 * 2;

    //    pushStyle();
    //    fill(#E3B128, t);
    //    ellipse(width_2, height_2, q, q);
    //    popStyle();

    bellCounter++;
    if (bellCounter >= s)
    {
      console.log("bell!!");
      bellCounter=0;
    }
    if (bellCounter*7 < width)
    {
      pushStyle();
      fill(#E3B128, t);
      ellipse(width_2, height_2, bellCounter*10, bellCounter*10);
      popStyle();
    }
  }



  void drawString(int x, int y)
  {
//    int sw = (int)map(y, 0, height, 2, 1);
    int sw = (isFxEnabled)? 4: 1;
    float st = map(y, 0, height, 50, 5);
    float q = map(x, 0, width, 0.1, 2)*v1;

    pushStyle();
    strokeWeight(sw);
    stroke(#9F4757, st);
    noFill();
    translate(0,-50);
    for (int i=1; i<=5; i++)
    {
      translate(0,i*10);
      bezier(-10, height_2, -10*q, height_2+10*q, 
      width+10*q, height_2-10*q, width+10, height_2);
    }
    popStyle();
  }
}

