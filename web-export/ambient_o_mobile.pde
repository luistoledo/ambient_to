/*
@pjs pauseOnBlur=true; 
@pjs crisp=false;
@pjs preload="Amitayus_Mandala.jpeg,beat.mp3,bell_15402.mp3,bird/bird_0.png,bird/bird_1.png,bird/bird_10.png,bird/bird_11.png,bird/bird_2.png,bird/bird_3.png,bird/bird_4.png,bird/bird_5.png,bird/bird_6.png,bird/bird_7.png,bird/bird_8.png,bird/bird_9.png,brown-noise.wav,chat/movie0.jpg,chat/movie1.jpg,chat/movie10.jpg,chat/movie11.jpg,chat/movie12.,pg,chat/movie13.jpg,chat/movie14.jpg,chat/movie15.jpg,chat/movie16.jpg,chat/movie17.jpg,chat/movie18.jpg,chat/,ovie19.jpg,chat/movie2.jpg,chat/movie20.jpg,chat/movie3.jpg,chat/movie4.jpg,chat/movie5.jpg,chat/movie6.jpg,chat/,ovie7.jpg,chat/movie8.jpg,chat/movie9.jpg,chat-women.mp3,chat-women.wav,frogs.mp3,mantra_20289.mp3,nature.mp3,om_15363.mp3,rain.mp3,string.mp3";
 */

/*******************
ambient_o
---------

A relaxing sounds & visuals generator for creative people.

This project generates a visual-sound combination that can
provoke curiosity at first, but it will relax the user as well.

Creative people enjoy being inspired, so the challenge of this 
project is to satisfy those people providing a system with lot 
of combinations to explore.
This toy also tries to push the user to achieve a system-combination 
where his personal satisfaction cause relaxation, because of 
the curated animations & sounds.

Made with Processing, using the javascript mode.
First version for the coursera program: "Creative Programming for Digital Media & Mobile Apps" on July 2013
Tracked on: https://bitbucket.org/luistoledo/ambient_to
********************/


Button b1;
Button b5;
public ArrayList<Button> buttons;
Boolean isGuiVisible = true;
Boolean isFxEnabled = false;

void setup()
{
  size(480, 320);
  frameRate(30);

  width_2 = width/2;
  height_2 = height/2;

  window.s = this;

  visuals = new Visuals();

  buttons = new ArrayList<Button>();

  b1 = new Button(264, 210, 60, 40);
  b1.loadDictionary(createSoundDictionary());
  b1.playSound();
  buttons.add(b1);

  b1 = new Button(154, 210, 60, 40);
  b1.loadDictionary(createSoundDictionary());
  b1.playSound();
  buttons.add(b1);

  b1 = new Button(154, 80, 60, 40);
  b1.loadDictionary(createSoundDictionary());
  b1.playSound();
  buttons.add(b1);

  b1 = new Button(264, 80, 60, 40);
  b1.loadDictionary(createSoundDictionary());
  b1.playSound();
  buttons.add(b1);
}

ArrayList createSoundDictionary()
{
  ArrayList<Sound> dic = new ArrayList<Sound>();
  dic.add(new Sound("", color(#C0C0C0), "silence.mp3", null));
  dic.add(new Sound("rain", color(#86978E), "rain.mp3", visuals.drawRain));
  dic.add(new Sound("frogs", color(#6F989D), "frogs.mp3", null));
  dic.add(new Sound("noise", color(#86978E), "brown-noise.wav", visuals.drawNoise));
  dic.add(new Sound("beat", color(#ACA690), "beat.mp3", null));
  dic.add(new Sound("nature", color(#86978E), "nature.mp3", visuals.drawBird));
  dic.add(new Sound("string", color(#ACA690), "string.mp3", visuals.drawString));
  dic.add(new Sound("bell", color(#6F989D), "bell_15402.mp3", visuals.drawBell));
  dic.add(new Sound("om", color(#86978E), "om_15363.mp3", visuals.drawOm));
  dic.add(new Sound("song", color(#ACA690), "mantra_20289.mp3", visuals.drawOm));
  return dic;
}

void draw()
{
  calculateFXFactors();

  // Adjust the clearscreen alpha for FX and noFX modes 
  int fillAlpha = isFxEnabled?5:50;
  fill(200+vc, fillAlpha);
  rect(0, 0, width, height);

  drawVisuals();
  
  if (isGuiVisible)
  {
    drawGUI();
  }
  
  if (isFxEnabled)
  {
      filter(DILATE, 0);
  }
}

void drawVisuals()
{
  for (int i=0; i<=buttons.size(); i++)
  {
    if (buttons.get(i) != null)
    {
      Button b = buttons.get(i);
      b.drawVisuals();
    }
  }
}

// Precalculate some fx factors to use them guiltiness
void calculateFXFactors()
{
  f_f = frameCount % frameRate;
  f_20 = frameCount/20;
  f_50 = frameCount/50;
  v0 = sin(f_20);
  v00 = 1+sin(f_20);
  v1 = 4*sin( f_20 );
  v2 = 4*cos( f_20 );
  v3 = 2*sin( f_50 );
  vc = 80*sin(f_50);
  vd = 0.1 * cos(f_20);
  t5 = tan(f_50);
}  


void drawGUI()
{

  for (int i=0; i<=buttons.size(); i++)
  {
    
    if (buttons.get(i) != null)
      if (buttons.get(i).constructor.name == "Button" )
      {
        Button b = buttons.get(i); 

        bcx= b.x+b.w/2;
        bcy= b.y+b.h/2;

        // Adjust the line weight for FX and noFX modes 
        int lineWeight = isFxEnabled?3:1;
        
        strokeWeight(lineWeight);
        stroke(0, 86, 121, 20);
        noFill();

        bezier(bcx, bcy, bcx+v1, bcy+v2, 
        width_2+v3, height_2+v3, width_2, height_2);

        bezier(bcx+v2, bcy+v2, bcx+v1, bcy+v2, 
        0+v1, 0+v3, -10-v2, -10-v1);
        bezier(bcx, bcy, bcx+v2, bcy+v1, 
        width+v2, height+v3, width, height);
        bezier(bcx, bcy, bcx+v2, bcy+v3, 
        width+v2, 0+v3, width, 0);
        bezier(bcx, bcy, bcx+v2, bcy+v2, 
        0+v2, height+v3, 0, height);

        // Connects the current button to the next one
        if (i<buttons.size()-1) {
          bbcx = buttons.get(i+1).x+buttons.get(i+1).w/2;
          bbcy = buttons.get(i+1).y+buttons.get(i+1).h/2;
          bezier(bcx, bcy, bcx+v3, bcy+v1, 
          bbcx+v2, bbcy+v1, bbcx, bbcy);

          // If current button is the firstone, lets make the connection with the lastone
          if (i==0) {
            bbcx = buttons.get(buttons.size()-1).x+buttons.get(0).w/2;
            bbcy = buttons.get(buttons.size()-1).y+buttons.get(0).h/2;
            bezier(bcx, bcy, bcx+v3, bcy+v1, 
            bbcx+v2, bbcy+v1, bbcx, bbcy);
          }
        }

        // Draws the button
        b.draw();
        fill(10);
      }
  }
}

void mousePressed()
{
  if (isGuiVisible)
  {
    for (int i=0; i<=buttons.size(); i++)
    {
      if (buttons.get(i) != null)
        if (buttons.get(i).constructor.name == "Button" )
        {
          if (buttons.get(i).isIn(mouseX, mouseY)) {
            buttons.get(i).pressed(mouseX, mouseY);
          }
        }
    }
  }
}

void mouseReleased()
{
  if (isGuiVisible)
  {
    isGuiVisible = false;
    for (int i=0; i<=buttons.size(); i++)
    {
      if (buttons.get(i) != null)
        if (buttons.get(i).constructor.name == "Button" )
        {
          if (buttons.get(i).isIn(mouseX, mouseY)) {
            buttons.get(i).released();
            isGuiVisible = true;
          }
        }
    }
  }  
  else
  {
    isGuiVisible=true;
  }
}

void mouseDragged()
{
  if (isGuiVisible)
  {
    for (int i=0; i<=buttons.size(); i++)
    {
      if (buttons.get(i) != null)
        if (buttons.get(i).constructor.name == "Button" )
        {
          if (buttons.get(i).isIn(mouseX, mouseY) && buttons.get(i).wasPressed) {
            buttons.get(i).dragged(mouseX, mouseY);
          }
        }
    }
  }
}

void keyReleased()
{
  if (key == 'f')
  {
    isFxEnabled = !isFxEnabled;
  }
}
class Button
{

  int x, y, w, h, r;
  int state;
  color fillColor;
  color strokeColor;
  ArrayList<Sound> sounds;
  int currentSound = 0;
  String name;
  color col;
  color normalBorderColor;
  String soundFile;
  var c = 0; // counter for the display function

  Button (int ix, int iy, int iw, int ih)
  {
    r = 40;
    x = ix;
    y = iy;
    w = iw;
    h = ih;
    fillColor = color(204, 153, 0);
    strokeColor = color(240, 240, 240);
    normalBorderColor = color(255, 240); 
  }

  void loadDictionary(ArrayList<Sound> dic)
  {
    sounds = new ArrayList<Sound>();
    for (int i=0; i<=dic.size()-1; i++)
    {
      sounds.add(dic.get(i));
    }
    fillColor = color(<Sound>sounds.get(currentSound).col);
  }
  
  void clearDictionary()
  {
    sounds = new ArrayList<Sound>();
    name = ""; 
    soundFile = ""; 
    col = color(100, 100, 100);
  }

  void draw()
  {
    pushStyle();
    fill (fillColor, 80);
    stroke(strokeColor, 160); //stroke(220);
    rect(x, y, w, h, r);
    fill(250);   
    textSize(12); 
    textAlign(CENTER, CENTER);
    text(name, x, y, w, h-4);
    popStyle();
  }
  
  void drawVisuals()
  {
    if (sounds.get(currentSound).display)
    {
      c = sounds.get(currentSound).display(x,y,c) || c;
    }
  }

  Boolean isIn(int px, int py)
  {
    if (px > x && px < x+w) {
      if (py > y && py < y+h) {
        return true;
      }
    }
    return false;
  }

  Boolean wasPressed = false;
  int px, py;
  void pressed(int ix, int iy)
  {
    px = x-ix; 
    py = y-iy;
    wasPressed = true;
    wasDragged = false;

//    strokeColor = normalBorderColor;
    strokeColor = col;
  }

  void released()
  {
    wasPressed = false;

    if (!wasDragged)
    {
      stopSound();
      if (currentSound >= sounds.size()-1) {
        currentSound=0;
      } 
      else {
        currentSound++;
      }
      playSound();
    }

    setPublicSoundData();
    fillColor = col;
    strokeColor = col;
  }

  Boolean wasDragged = false;
  void dragged(int ix, int iy)
  {
    wasDragged = true;
    x = ix + px;
    y = iy + py;
    updateSoundSpeed();
    updateSoundVolume();
  }

  void setPublicSoundData()
  {
    name = <Sound>sounds.get(currentSound).name;
    col = <Sound>sounds.get(currentSound).col;
    soundFile = <Sound>sounds.get(currentSound).file;
  }
    
  void stopSound()
  {
    sounds.get(currentSound).sound.stop();
    sounds.get(currentSound).sound.cue(0);
    console.log("stop: " + sounds.get(currentSound).name);
  }

  void updateSoundSpeed()
  {
    float speed = map(x+w/2, 0, width, 0.2, 1.5); 
    sounds.get(currentSound).sound.speed(speed);
  }
  
  void updateSoundVolume()
  {
    float volume = map(y+h/2, 0, height, 1, -0.1); 
    sounds.get(currentSound).sound.volume(volume);    
  }
  
  void playSound()
  {
    updateSoundVolume();
    updateSoundSpeed();
    sounds.get(currentSound).sound.play();
    console.log("play: " + sounds.get(currentSound).name);
  }
}

Maxim maxim;



class Sound
{
  String name;
  color col;
  String file;
  AudioPlayer sound;
  function display;
  
  Sound(String iname, color icol, String ifile, function idisplay)
  {
    name = iname;
    col = icol;
    file = ifile;
    display = idisplay;
    
    sound = new Maxim(this).loadFile(ifile);
    sound.setLooping(true);
  }
}
class Visuals
{

  Visuals()
  {
    setupChat();
    setupBird();
    setupOm();
  }

  static void drawRain(int x, int y, var c)
  {
    int q = map(y, 0, height, 4, 0.5) * map(x, 0, width, 0.5, 4);
    pushStyle();
    fill(250);
    for (int i=0; i<=q; i++)
    {
      int dx = int(random(width));
      int dy = height - int(random(0, 20));

      stroke(color(int(random(50, 250))));
      strokeWeight(1);

      line(dx, 0, dx, dy);

      noStroke();
      ellipse(dx+1, dy+1, 2, 2);
    }
    popStyle();
  }

  static void drawNoise(int x, int y, var c)
  {
    float noiseSclae = 0.02;
    int q = map(y, 0, height, 40, 0);
    if (q > f_f) { //not so sure..but..
      pushStyle();
      for (int i=0; i<=q; i++)
      {
        int c = int(random(0, 255));
        int x = int(random(0, width));
        int y = int(random(0, height));
        fill(c, 230);
        noStroke();
        rect(x, y, 3, 3);
        //      point(x, y);
      }
      popStyle();
    }
  }

  PImage[] chatImages = new PImage[20];
  void setupChat()
  {
    for (int i=0; i<=20; i++)
    {
      chatImages[i] = loadImage("chat/movie"+i+".jpg");
    }
  }
  void drawChat(int x, int y, var o)
  {
    int t = map(y, 0, height, 60, 0);
    float q = map(x, 0, width, 1.2, .034);
    if (ceil(frameCount % (frameRate*q)) == 1.0)
    {
      o++;
      if (o > 18) { 
        o = 1;
      }
    }
    pushStyle();
    tint(255, t);
    image(chatImages[o], 0, 0, width, height);
    popStyle();

    return o;
  }


  PImage[] birdImages = new PImage[12];
  int currentBird = 0;
  int birdCounter = 0;
  void setupBird()
  {
    for (int i=0; i<=11; i++)
    {
      birdImages[i] = loadImage("bird/bird_"+i+".png");
    }
  }
  void drawBird(int x, int y, var o)
  {
    int t = map(y, 0, height, 100, 0);
    var q = map (x, 0, width, 1, 0.01) * frameRate;

    birdCounter++;
    if (birdCounter >= q)
    {
      birdCounter = 0;
      currentBird = (currentBird >= birdImages.length()-1) ? 1 : (currentBird+1);
    }

    pushStyle();
    tint(255, t);
    scale(4.0);
    image(birdImages[currentBird], 0, 0);
    popStyle();

    return o;
  }


  PImage omImage;
  void setupOm()
  {
    omImage = loadImage("Amitayus_Mandala.jpeg");
  }
  void drawOm(int x, int y, var c)
  {
    int t = map(y, 0, height, 60, 0);
    float r = map(x, 0, width, 0.05, 1) * v0;
    float s = (map(x, 0, width, 1, 2) * v00) + 1.5;

    pushMatrix();
    imageMode(CENTER);
    translate(width_2, height_2);
    rotate(r);
    scale(s);
    tint(255, t);
    image(omImage, 0, 0);
    popMatrix();
    imageMode(CORNER);
  }


  int bellCounter = 0;
  void drawBell(int x, int y)
  {
    float t = map(y, 0, height, 40, 5);
    //    float q = map(x, 0, width, 0.2, 1.5) * frameRate / 4 * t5;
    float s = map(x, 0, width, 1.5, 0.2) * frameRate * 8.05 * 2;

    //    pushStyle();
    //    fill(#E3B128, t);
    //    ellipse(width_2, height_2, q, q);
    //    popStyle();

    bellCounter++;
    if (bellCounter >= s)
    {
      console.log("bell!!");
      bellCounter=0;
    }
    if (bellCounter*7 < width)
    {
      pushStyle();
      fill(#E3B128, t);
      ellipse(width_2, height_2, bellCounter*10, bellCounter*10);
      popStyle();
    }
  }



  void drawString(int x, int y)
  {
//    int sw = (int)map(y, 0, height, 2, 1);
    int sw = (isFxEnabled)? 4: 1;
    float st = map(y, 0, height, 50, 5);
    float q = map(x, 0, width, 0.1, 2)*v1;

    pushStyle();
    strokeWeight(sw);
    stroke(#9F4757, st);
    noFill();
    translate(0,-50);
    for (int i=1; i<=5; i++)
    {
      translate(0,i*10);
      bezier(-10, height_2, -10*q, height_2+10*q, 
      width+10*q, height_2-10*q, width+10, height_2);
    }
    popStyle();
  }
}


