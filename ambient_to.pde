/*
@pjs pauseOnBlur=true; 
@pjs crisp=false;
@pjs preload="Amitayus_Mandala.jpeg,beat.mp3,bell_15402.mp3,bird/bird_0.png,bird/bird_1.png,bird/bird_10.png,bird/bird_11.png,bird/bird_2.png,bird/bird_3.png,bird/bird_4.png,bird/bird_5.png,bird/bird_6.png,bird/bird_7.png,bird/bird_8.png,bird/bird_9.png,brown-noise.wav,chat/movie0.jpg,chat/movie1.jpg,chat/movie10.jpg,chat/movie11.jpg,chat/movie12.,pg,chat/movie13.jpg,chat/movie14.jpg,chat/movie15.jpg,chat/movie16.jpg,chat/movie17.jpg,chat/movie18.jpg,chat/,ovie19.jpg,chat/movie2.jpg,chat/movie20.jpg,chat/movie3.jpg,chat/movie4.jpg,chat/movie5.jpg,chat/movie6.jpg,chat/,ovie7.jpg,chat/movie8.jpg,chat/movie9.jpg,chat-women.mp3,chat-women.wav,frogs.mp3,mantra_20289.mp3,nature.mp3,om_15363.mp3,rain.mp3,string.mp3";
 */

/*******************
ambient_o
---------

A relaxing sounds & visuals generator for creative people.

This project generates a visual-sound combination that can
provoke curiosity at first, but it will relax the user as well.

Creative people enjoy being inspired, so the challenge of this 
project is to satisfy those people providing a system with lot 
of combinations to explore.
This toy also tries to push the user to achieve a system-combination 
where his personal satisfaction cause relaxation, because of 
the curated animations & sounds.

Made with Processing, using the javascript mode.
First version for the coursera program: "Creative Programming for Digital Media & Mobile Apps" on July 2013
Source code: https://bitbucket.org/luistoledo/ambient_to
Video: https://vimeo.com/71239839
Try it live on: http://digitalcth.com/ambient_o
********************/


Button b1;
Button b5;
public ArrayList<Button> buttons;
Boolean isGuiVisible = true;
Boolean isFxEnabled = true;

void setup()
{
  size(480, 320);
  frameRate(30);

  width_2 = width/2;
  height_2 = height/2;

  window.s = this;

  visuals = new Visuals();

  buttons = new ArrayList<Button>();

  b1 = new Button(264, 210, 60, 40);
  b1.loadDictionary(createSoundDictionary());
  b1.playSound();
  buttons.add(b1);

  b1 = new Button(154, 210, 60, 40);
  b1.loadDictionary(createSoundDictionary());
  b1.playSound();
  buttons.add(b1);

  b1 = new Button(154, 80, 60, 40);
  b1.loadDictionary(createSoundDictionary());
  b1.playSound();
  buttons.add(b1);

  b1 = new Button(264, 80, 60, 40);
  b1.loadDictionary(createSoundDictionary());
  b1.playSound();
  buttons.add(b1);
}

ArrayList createSoundDictionary()
{
  ArrayList<Sound> dic = new ArrayList<Sound>();
  dic.add(new Sound("", color(#C0C0C0), "silence.mp3", null));
  dic.add(new Sound("rain", color(#86978E), "rain.mp3", visuals.drawRain));
  dic.add(new Sound("frogs", color(#6F989D), "frogs.mp3", null));
  dic.add(new Sound("noise", color(#86978E), "brown-noise.wav", visuals.drawNoise));
  dic.add(new Sound("beat", color(#ACA690), "beat.mp3", null));
  dic.add(new Sound("nature", color(#86978E), "nature.mp3", visuals.drawBird));
  dic.add(new Sound("string", color(#ACA690), "string.mp3", visuals.drawString));
  dic.add(new Sound("bell", color(#6F989D), "bell_15402.mp3", visuals.drawBell));
  dic.add(new Sound("om", color(#86978E), "om_15363.mp3", visuals.drawOm));
  dic.add(new Sound("chat", color(#6F989D), "chat-women.wav", visuals.drawChat));
  dic.add(new Sound("song", color(#ACA690), "mantra_20289.mp3", visuals.drawOm));
  return dic;
}

void draw()
{
  calculateFXFactors();

  // Adjust the clearscreen alpha for FX and noFX modes 
  int fillAlpha = isFxEnabled?5:50;
  fill(200+vc, fillAlpha);
  rect(0, 0, width, height);

  drawVisuals();
  
  if (isGuiVisible)
  {
    drawGUI();
  }
  
  if (isFxEnabled)
  {
      filter(DILATE, 0);
  }
}

void drawVisuals()
{
  for (int i=0; i<=buttons.size(); i++)
  {
    if (buttons.get(i) != null)
    {
      Button b = buttons.get(i);
      b.drawVisuals();
    }
  }
}

// Precalculate some fx factors to use them guiltiness
void calculateFXFactors()
{
  f_f = frameCount % frameRate;
  f_20 = frameCount/20;
  f_50 = frameCount/50;
  v0 = sin(f_20);
  v00 = 1+sin(f_20);
  v1 = 4*sin( f_20 );
  v2 = 4*cos( f_20 );
  v3 = 2*sin( f_50 );
  vc = 80*sin(f_50);
  vd = 0.1 * cos(f_20);
  t5 = tan(f_50);
}  


void drawGUI()
{

  for (int i=0; i<=buttons.size(); i++)
  {
    
    if (buttons.get(i) != null)
      if (buttons.get(i).constructor.name == "Button" )
      {
        Button b = buttons.get(i); 

        bcx= b.x+b.w/2;
        bcy= b.y+b.h/2;

        // Adjust the line weight for FX and noFX modes 
        int lineWeight = isFxEnabled?3:1;
        
        strokeWeight(lineWeight);
        stroke(0, 86, 121, 20);
        noFill();

        bezier(bcx, bcy, bcx+v1, bcy+v2, 
        width_2+v3, height_2+v3, width_2, height_2);

        bezier(bcx+v2, bcy+v2, bcx+v1, bcy+v2, 
        0+v1, 0+v3, -10-v2, -10-v1);
        bezier(bcx, bcy, bcx+v2, bcy+v1, 
        width+v2, height+v3, width, height);
        bezier(bcx, bcy, bcx+v2, bcy+v3, 
        width+v2, 0+v3, width, 0);
        bezier(bcx, bcy, bcx+v2, bcy+v2, 
        0+v2, height+v3, 0, height);

        // Connects the current button to the next one
        if (i<buttons.size()-1) {
          bbcx = buttons.get(i+1).x+buttons.get(i+1).w/2;
          bbcy = buttons.get(i+1).y+buttons.get(i+1).h/2;
          bezier(bcx, bcy, bcx+v3, bcy+v1, 
          bbcx+v2, bbcy+v1, bbcx, bbcy);

          // If current button is the firstone, lets make the connection with the lastone
          if (i==0) {
            bbcx = buttons.get(buttons.size()-1).x+buttons.get(0).w/2;
            bbcy = buttons.get(buttons.size()-1).y+buttons.get(0).h/2;
            bezier(bcx, bcy, bcx+v3, bcy+v1, 
            bbcx+v2, bbcy+v1, bbcx, bbcy);
          }
        }

        // Draws the button
        b.draw();
        fill(10);
      }
  }
}

void mousePressed()
{
  if (isGuiVisible)
  {
    for (int i=0; i<=buttons.size(); i++)
    {
      if (buttons.get(i) != null)
        if (buttons.get(i).constructor.name == "Button" )
        {
          if (buttons.get(i).isIn(mouseX, mouseY)) {
            buttons.get(i).pressed(mouseX, mouseY);
          }
        }
    }
  }
}

void mouseReleased()
{
  if (isGuiVisible)
  {
    isGuiVisible = false;
    for (int i=0; i<=buttons.size(); i++)
    {
      if (buttons.get(i) != null)
        if (buttons.get(i).constructor.name == "Button" )
        {
          if (buttons.get(i).isIn(mouseX, mouseY)) {
            buttons.get(i).released();
            isGuiVisible = true;
          }
        }
    }
  }  
  else
  {
    isGuiVisible=true;
  }
}

void mouseDragged()
{
  if (isGuiVisible)
  {
    for (int i=0; i<=buttons.size(); i++)
    {
      if (buttons.get(i) != null)
        if (buttons.get(i).constructor.name == "Button" )
        {
          if (buttons.get(i).isIn(mouseX, mouseY) && buttons.get(i).wasPressed) {
            buttons.get(i).dragged(mouseX, mouseY);
          }
        }
    }
  }
}

void keyReleased()
{
  if (key == 'f')
  {
    isFxEnabled = !isFxEnabled;
  }
}
