Maxim maxim;



class Sound
{
  String name;
  color col;
  String file;
  AudioPlayer sound;
  function display;
  
  Sound(String iname, color icol, String ifile, function idisplay)
  {
    name = iname;
    col = icol;
    file = ifile;
    display = idisplay;
    
    sound = new Maxim(this).loadFile(ifile);
    sound.setLooping(true);
  }
}
